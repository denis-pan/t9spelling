﻿namespace T9Spelling.Tools
{
    using System;

    /// <summary>
    ///     Helper class for work with console
    /// </summary>
    internal static class ConsoleUtils
    {
        /// <summary>
        ///     Print message to console and read string.
        /// </summary>
        /// <param name="msg">Message for printing.</param>
        /// <returns>String from console.</returns>
        public static string ReadFromConsole(string msg = "")
        {
            Console.WriteLine(msg);
            return Console.ReadLine();
        }

        /// <summary>
        ///     Print message to console and read data from console.
        /// </summary>
        /// <typeparam name="T">Any convertible type(implement IConvertible).</typeparam>
        /// <param name="msg">Message for printing.</param>
        /// <returns>String from console converted to T type.</returns>
        public static T ReadFromConsole<T>(string msg = "") where T : IConvertible
        {
            Console.WriteLine(msg);
            return (T) Convert.ChangeType(Console.ReadLine(), typeof(T));
        }

        /// <summary>
        ///     Print message to console.
        /// </summary>
        /// <param name="msg">String with message.</param>
        /// <param name="foregroundColor">[optional]Font color(default is yellow).</param>
        public static void PrintInfo(string msg, ConsoleColor foregroundColor = ConsoleColor.Yellow)
        {
            Console.ForegroundColor = foregroundColor;
            Console.WriteLine(msg);
            Console.ResetColor();
        }

        /// <summary>
        ///     Print error message to console.
        /// </summary>
        /// <param name="msg">String with error message.</param>
        public static void PrintError(string msg)
        {
            PrintInfo($"An unhandled exception occured: {msg}", ConsoleColor.Red);
        }


        /// <summary>
        ///     Print dialog message to console and get confirm from user.(yes/no)
        /// </summary>
        /// <param name="msg">String with message.</param>
        /// <returns>User confirmation.</returns>
        public static bool PrintDialog(string msg)
        {
            ConsoleKey response;
            do
            {
                Console.Write($"{msg} [y/n]: ");
                response = Console.ReadKey(false).Key;

                if (response != ConsoleKey.Enter)
                {
                    Console.WriteLine();
                }

            } 
            while (response != ConsoleKey.Y && response != ConsoleKey.N);

            return response == ConsoleKey.Y;
        }
    }
}
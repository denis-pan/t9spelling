﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("T9SpellingTests")]
namespace T9Spelling.Tools
{
    using System.IO;
    using System.Runtime.Serialization.Json;

    /// <summary>
    ///     Helper for serialize and deserialize models to json file.
    /// </summary>
    internal static class FileParser
    {
        /// <summary>
        ///     Serialize model to file.
        /// </summary>
        /// <typeparam name="T">Any serializable model.</typeparam>
        /// <param name="obj">Object of model.</param>
        /// <param name="file">Name and path for output json file.</param>
        public static void Serialize<T>(T obj, string file)
        {
            var jsonFormatter = new DataContractJsonSerializer(typeof(T));

            using (var fs = new FileStream(file, FileMode.Create))
            {
                jsonFormatter.WriteObject(fs, obj);
            }
        }

        /// <summary>
        ///     Deserialize model from json file.
        /// </summary>
        /// <typeparam name="T">Any serializable model.</typeparam>
        /// <param name="file">Name and path of input json file.</param>
        /// <returns>Deserialized model.</returns>
        public static T Deserialize<T>(string file)
        {
            var jsonFormatter = new DataContractJsonSerializer(typeof(T));
            using (var fs = new FileStream(file, FileMode.Open))
            {
                var result = (T) jsonFormatter.ReadObject(fs);
                return result;
            }
        }
    }
}
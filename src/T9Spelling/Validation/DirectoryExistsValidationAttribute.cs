﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("T9SpellingTests")]

namespace T9Spelling.Validation
{
    using System.ComponentModel.DataAnnotations;
    using System.IO;

    /// <summary>
    ///     Validation attribute for check is directory of file exist.
    /// </summary>
    internal class DirectoryExistsValidationAttribute : ValidationAttribute
    {
        /// <summary>
        ///     Validate file path.
        /// </summary>
        /// <param name="canBeNull">If path is null return this bool value</param>
        public DirectoryExistsValidationAttribute(bool canBeNull = false)
        {
            CanBeNull = canBeNull;
        }

        private bool CanBeNull { get; }

        /// <inheritdoc />
        public sealed override bool IsValid(object value)
        {
            if (value == null)
            {
                return CanBeNull;
            }

            var file = new FileInfo(value.ToString());
            if (file.Directory != null && Directory.Exists(file.Directory.FullName))
            {
                return true;
            }
            return false;
        }
    }
}
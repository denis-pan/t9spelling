﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using T9Spelling.Models;
using T9SpellingUtils.Types;

[assembly: InternalsVisibleTo("T9SpellingTests")]
namespace T9Spelling.Validation
{
    internal class KeypadValidationAttribute : ValidationAttribute
    {
        private char[] ValidKeys = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '*', '#'};

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ErrorMessage = ErrorMessageString;
            var keypad = (List<SerializableButton>)value;

            var buttons = keypad.Select(x => x.Key).ToList();
            var symbols = keypad
                              .Where(x => x.Symbols != null)
                              .SelectMany(x => x.Symbols)
                              .ToList();

            return HasDuplicate(buttons) || HasDuplicate(symbols) || buttons.Any(x => !ValidKeys.Contains(x))
                ? new ValidationResult(ErrorMessage)
                : ValidationResult.Success;
        }

        private bool HasDuplicate<T> (List<T> list) 
        {
            return list.GroupBy(n => n).Any(c => c.Count() > 1);
        }
    }
}

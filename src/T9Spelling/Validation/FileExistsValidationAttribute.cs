﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("T9SpellingTests")]

namespace T9Spelling.Validation
{
    using System.ComponentModel.DataAnnotations;
    using System.IO;

    /// <summary>
    ///     Validation attribute for check is file exist.
    /// </summary>
    internal class FileExistsValidationAttribute : ValidationAttribute
    {
        /// <summary>
        ///     Validate path and name of the file.
        /// </summary>
        /// <param name="canBeNull">If path is null return this bool value.</param>
        public FileExistsValidationAttribute(bool canBeNull = false)
        {
            CanBeNull = canBeNull;
        }

        private bool CanBeNull { get; }

        /// <inheritdoc />
        public sealed override bool IsValid(object value)
        {
            if (value == null)
            {
                return CanBeNull;
            }

            string path = value.ToString();
            if (File.Exists(path))
            {
                return true;
            }

            return false;
        }
    }
}
﻿using System;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("T9SpellingTests")]
namespace T9Spelling.Validation
{
    internal class InputStringsValidationAttribute : ValidationAttribute
    {

        private readonly string _comparisonProperty;

        public InputStringsValidationAttribute(string comparisonProperty)
        {
            _comparisonProperty = comparisonProperty;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ErrorMessage = ErrorMessageString;
            var currentValue = (IList)value;

            var property = validationContext.ObjectType.GetProperty(_comparisonProperty);

            if (property == null)
                throw new ArgumentException("Property with this name not found");

            var compareValue = (int)property.GetValue(validationContext.ObjectInstance);

            if (currentValue.Count > compareValue)
                return new ValidationResult(ErrorMessage);

            return ValidationResult.Success;
        }
    }
}

﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("T9SpellingTests")]
namespace T9Spelling.Core
{
    using T9SpellingUtils.Models;
    using util = T9Spelling.Tools.ConsoleUtils;
    internal sealed class WorkerUI : Worker
    {
        /// <summary>
        ///     Worker for CLI application mode(when user enter input data from console).
        /// </summary>
        public WorkerUI() : base()
        {
            Keyboard = Keyboard.Default;
            CaseSensitive = false;
            Decode = false;
        }

        /// <inheritdoc />
        public override bool Init()
        {
            Reader = new ConsoleController();
            Writer = new ConsoleController();
            Converter = new T9SpellingUtils.Converter(Keyboard, CaseSensitive);

            return true;
        }

        /// <inheritdoc />
        public override void Run()
        {
            bool exit = false;
            while (!exit)
            {
                Process();
                exit = util.PrintDialog("Exit program?");
            }
        }
    }
}

﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("T9SpellingTests")]
namespace T9Spelling.Core
{
    using System;
    using Models;
    using Tools;
    using T9SpellingUtils;
    using T9SpellingUtils.Models;
    using util = Tools.ConsoleUtils;
    /// <summary>
    ///     Worker for CLI application mode(when program run with input arguments from command line).
    /// </summary>
    internal sealed class WorkerCLI : Worker
    {
        /// <summary>
        ///     Arguments from command line.
        /// </summary>
        private readonly string[] _argsList;

        /// <summary>
        ///     Constructor for CLI worker object.
        /// </summary>
        /// <param name="args">Arguments from command line.</param>
        public WorkerCLI(string[] args)
        {
            _argsList = args;
        }

        /// <summary>
        ///     Arguments from command line as object.
        /// </summary>
        public InputArguments Arguments { get; set; }

        /// <inheritdoc />
        public override bool Init()
        {
            Arguments = new InputArguments(_argsList);

            if (Arguments.Help || !IsValid(Arguments))
            {
                ShowHelp();
                return false;
            }

            if (!string.IsNullOrWhiteSpace(Arguments.Keyboard))
            {
                var inputKeyboard = FileParser.Deserialize<SerializableKeyboard>(Arguments.Keyboard);
                if (IsValid(inputKeyboard))
                {
                    Keyboard = inputKeyboard;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                Keyboard = Keyboard.Default;
            }

            CaseSensitive = Arguments.CaseSensitive;
            Decode = Arguments.Decode;

            Converter = new Converter(Keyboard, CaseSensitive);
            Reader = new FileController(Arguments.InputFile);
            Writer = string.IsNullOrEmpty(Arguments.OutputFile)
                ? (IDataWriter) new ConsoleController()
                : (IDataWriter) new FileController(Arguments.OutputFile);

            return true;
        }

        /// <inheritdoc />
        public override void Run()
        {
            Process();
        }

        /// <summary>
        ///     Print help message to console.
        /// </summary>
        private void ShowHelp()
        {
            util.PrintInfo(InputArguments.GetHelpMessage(), ConsoleColor.Blue);
        }
    }
}
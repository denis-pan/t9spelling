﻿namespace T9Spelling.Core
{
    using System.Collections.Generic;
    using Models;
    using util = Tools.ConsoleUtils;

    /// <summary>
    ///     Console data provider.
    /// </summary>
    internal sealed class ConsoleController : IDataReader, IDataWriter
    {
        /// <summary>
        ///     Get input data from console.
        /// </summary>
        /// <returns>Input data entered by user</returns>
        public IData GetData()
        {
            var n = util.ReadFromConsole<int>("Enter count of input strings(N): ");
            var inputStrings = new List<string>();

            for (var i = 0; i < n; i++) inputStrings.Add(util.ReadFromConsole($"Enter {i + 1} string: "));

            return new InputData(n, inputStrings);
        }

        /// <summary>
        ///     Print output data to console.
        /// </summary>
        public void WriteData(IData output)
        {
            for (var i = 0; i < output.N; i++) util.PrintInfo($"Case #{i + 1}: {output.Data[i]}");
        }
    }
}
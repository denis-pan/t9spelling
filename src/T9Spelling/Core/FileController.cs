﻿namespace T9Spelling.Core
{
    using System;
    using Models;
    using Tools;
    using util = Tools.ConsoleUtils;

    /// <summary>
    /// File data provider
    /// </summary>
    internal sealed class FileController : IDataReader, IDataWriter
    {
        private string FileName { get; set; }

        /// <summary>
        /// Create file data provider.
        /// </summary>
        /// <param name="fileName">Path and name of json file.</param>
        public FileController(string fileName)
        {
            FileName = fileName;
        }

        /// <summary>
        /// Get input data from file.
        /// </summary>
        /// <returns>Input data parsed from file.</returns>
        public IData GetData()
        {
            var input = FileParser.Deserialize<InputData>(FileName);
            return input;
        }

        /// <summary>
        /// Write output data to file.
        /// </summary>
        public void WriteData(IData data)
        {
            FileParser.Serialize((OutputData)data, FileName);
            util.PrintInfo("Success!", ConsoleColor.Green);
        }
    }
}

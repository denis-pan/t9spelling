﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("T9SpellingTests")]
namespace T9Spelling.Core
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Models;
    using T9SpellingUtils;
    using T9SpellingUtils.Models;
    using util = Tools.ConsoleUtils;

    /// <summary>
    ///     Abstract worker for converting and decoding messages.
    /// </summary>
    internal abstract class Worker : IWorker
    {
        /// <summary>
        ///     Converter for input data processing.
        /// </summary>
        protected Converter Converter { get; set; }

        /// <inheritdoc />
        public IDataReader Reader { get; set; }

        /// <inheritdoc />
        public IDataWriter Writer { get; set; }

        /// <inheritdoc />
        public Keyboard Keyboard { get; protected set; }

        /// <inheritdoc />
        public InputData InputData { get; protected set; }

        /// <inheritdoc />
        public OutputData OutputData { get; protected set; }

        /// <inheritdoc />
        public bool CaseSensitive { get; protected set; }

        /// <inheritdoc />
        public bool Decode { get; protected set; }

        /// <inheritdoc />
        public abstract bool Init();

        /// <inheritdoc />
        public abstract void Run();

        /// <summary>
        ///     Fabric method for creating a specific worker.
        /// </summary>
        /// <param name="args">Arguments from command line.</param>
        /// <returns>Specific worker(UI/CLI).</returns>
        public static IWorker CreateWorker(string[] args)
        {
            IWorker worker;
            if (args != null && args.Length > 0)
            {
                worker = new WorkerCLI(args);
            }
            else
            {
                worker = new WorkerUI();
            }

            return worker;
        }

        /// <summary>
        ///     Main process method. Method get, validate and process input data, then save result.
        /// </summary>
        protected void Process()
        {
            InputData = (InputData) Reader.GetData();
            if (InputData == null || !IsValid(InputData))
            {
                return;
            }
            var result = Decode ? Converter.CodeToMessage(InputData.Data) : Converter.MessageToCode(InputData.Data);
            OutputData = new OutputData(result);
            Writer.WriteData(OutputData);
        }

        /// <summary>
        ///     Check models with validation attribute and print errors.
        /// </summary>
        /// <typeparam name="T">Model with validated fields.</typeparam>
        /// <param name="obj">Validated object.</param>
        /// <returns>Result of validation.</returns>
        protected bool IsValid<T>(T obj)
        {
            var validationResult = new List<ValidationResult>();
            var validationContext = new ValidationContext(obj);
            if (Validator.TryValidateObject(obj, validationContext, validationResult, true))
            {
                return true;
            }

            foreach (var error in validationResult)
            {
                util.PrintError(error.ErrorMessage);
            }
            return false;
        }
    }
}
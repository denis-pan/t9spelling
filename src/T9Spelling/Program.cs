﻿using System;
using T9Spelling.Core;
using T9Spelling.Models;
using util = T9Spelling.Tools.ConsoleUtils;

namespace T9Spelling
{
    internal sealed class Program
    {
        /// <summary>
        ///     The entry point.
        /// </summary>
        private static void Main(string[] args)
        {
            try
            {
                IWorker worker = Worker.CreateWorker(args);
                bool initSuccess = worker.Init();
                if (initSuccess)
                {
                    worker.Run();
                }
            }
            catch (Exception e)
            {
                util.PrintError(e.Message);
                //TODO add logger
            }
        }
    }
}

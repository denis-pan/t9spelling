﻿namespace T9Spelling.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using T9SpellingUtils.Models;
    using T9SpellingUtils.Types;

    /// <summary>
    ///     Button serializable model.
    /// </summary>
    [DataContract]
    public class SerializableButton
    {
        private List<char> _symbols;

        /// <summary>
        ///     Type of button.
        /// </summary>
        public ButtonType Type { get; set; } = ButtonType.Standard;

        /// <summary>
        ///     Key of button.
        /// </summary>
        [DataMember]
        public char Key { get; set; }

        /// <summary>
        ///     List of symbols for button(if exist)
        /// </summary>
        [DataMember]
        public List<char> Symbols
        {
            get => _symbols;
            set { _symbols = value.Select(s => char.ToLower(s)).ToList(); }
        }

        /// <summary>
        ///     Used for serialization button type.
        /// </summary>
        [DataMember(Name = "Type")]
        private string SerializableType
        {
            get => Type.ToString();
            set => Type = Enum.TryParse(value, true, out ButtonType t) ? t : ButtonType.Standard;
        }

        public static explicit operator Button(SerializableButton button)
        {
            if (button != null)
                switch (button.Type)
                {
                    case ButtonType.Standard:
                        return new DigitButton(button.Key, button.Symbols);

                    case ButtonType.UpperCase:
                        return new SpecialButton(button.Key, button.Type);

                    default:
                        throw new NotImplementedException("Button type not found or not implemented");
                }
            throw new ArgumentException("Button can't be null");
        }
    }
}
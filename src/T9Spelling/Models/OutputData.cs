﻿namespace T9Spelling.Models
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    ///     Output data model.
    /// </summary>
    [DataContract]
    public sealed class OutputData : IData
    {
        /// <summary>
        ///     Create output data model from list of output messages.
        /// </summary>
        /// <param name="data">List of output messages.</param>
        public OutputData(List<string> data)
        {
            N = data.Count;
            Data = data;
        }

        /// <inheritdoc />
        [DataMember]
        public int N { get; private set; }

        /// <inheritdoc />
        [DataMember]
        public List<string> Data { get; private set; }
    }
}
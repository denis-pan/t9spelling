﻿namespace T9Spelling.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Runtime.Serialization;
    using T9SpellingUtils.Models;
    using T9SpellingUtils.Types;
    using Validation;

    /// <summary>
    ///     Keyboard serializable model.
    /// </summary>
    [DataContract]
    public class SerializableKeyboard
    {
        /// <summary>
        ///     Keyboard language.
        /// </summary>
        [Required(ErrorMessage = "Locale is require")]
        public LocaleType Locale { get; private set; }

        /// <summary>
        ///     Keyboard buttons.
        /// </summary>
        [Required(ErrorMessage = "Keypad is require")]
        [KeypadValidation(ErrorMessage = "Keys or symbols on keyboard are duplicated or invalid.")]
        [DataMember]
        public List<SerializableButton> Keypad { get; private set; }

        [DataMember(Name = "Locale")]
        private string SerializableLocale
        {
            get => Locale.ToString();
            set => Locale = Enum.TryParse(value, true, out LocaleType l) ? l : LocaleType.None;
        }

        public static implicit operator Keyboard(SerializableKeyboard keyboard)
        {
            return new Keyboard(keyboard.Locale, keyboard.Keypad.ConvertAll(b => (Button) b));
        }
    }
}
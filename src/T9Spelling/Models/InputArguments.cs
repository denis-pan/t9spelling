﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("T9SpellingTests")]
namespace T9Spelling.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using T9Spelling.Validation;

    /// <summary>
    ///     Arguments parsed from console.
    /// </summary>
    internal class InputArguments : CommandLineArguments
    {
        /// <summary>
        ///     Parse arguments from string array to argument object.
        /// </summary>
        /// <param name="args">Arguments from command line.</param>
        public InputArguments(string[] args)
            : base(args)
        {
        }

        /// <summary>
        ///     Name and path of json with input data.
        /// </summary>
        [Required(ErrorMessage = "Input file is require parameter.")]
        [FileExistsValidation(false, ErrorMessage = "Input file does not exist.")]
        public string InputFile => FormatFileName(this["input"] ?? this["i"]);

        /// <summary>
        ///     Name and path of json with keyboard model.
        /// </summary>
        [FileExistsValidation(true, ErrorMessage = "Keyboard file does not exist.")]
        public string Keyboard => FormatFileName(this["keyboard"] ?? this["k"]);

        /// <summary>
        ///     Name and path for output data.
        /// </summary>
        [DirectoryExistsValidation(true, ErrorMessage = "Directory of output file does not exist.")]
        public string OutputFile => FormatFileName(this["output"] ?? this["o"]);

        /// <summary>
        ///     Flag for display help.
        /// </summary>
        public bool Help => (this["help"] ?? this["h"]) != null;

        /// <summary>
        ///     Flag of case sensitive for converter.
        /// </summary>
        public bool CaseSensitive => (this["casesensitive"] ?? this["cs"]) != null;

        /// <summary>
        ///     Flag for switching converting type.
        /// </summary>
        public bool Decode => (this["decode"] ?? this["d"]) != null;

        /// <summary>
        ///     Get help message string.
        /// </summary>
        /// <returns>Help message.</returns>
        public static string GetHelpMessage()
        {
            return @"
Require arguments:
    -input(-i)      : Path to input JSON file.

Optional arguments:
    -help(-h)           : Show help message
    -keyboard(-k)       : Path to JSON file with keyboard definition.
    -output(-o)         : Path to output JSON file.
    -casesensitive(-cs) : Enable distinguishing lowercase letters from capital letters.
    -decode(-d)         : Decode input data to message.
";
        }

        /// <summary>
        ///     Add json extension to filename if not defined.
        /// </summary>
        /// <param name="file">Name and path to file.</param>
        /// <returns>New file name with json extension.</returns>
        private static string FormatFileName(string file)
        {
            if (string.IsNullOrEmpty(file))
            {
                return null;
            }
            return file.EndsWith(".json", System.StringComparison.OrdinalIgnoreCase) ? file : file + ".json";
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace T9Spelling.Models
{
    public abstract class CommandLineArguments
    {
        private readonly IDictionary<string, string> keyValueMap = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

        public int Count => keyValueMap.Count;

        protected string this[string key] => !keyValueMap.ContainsKey(key) ? null : keyValueMap[key];

        protected CommandLineArguments(params string[] args)
        {
            for (var i = 0; i < args.Length; i++)
            {
                var arg = args[i];
                if (IsKey(arg))
                {
                    if (IsNextArgAValue(args, i))
                    {
                        if (keyValueMap.ContainsKey(arg))
                        {
                            throw new ArgumentException($"the key {arg} is contained twice ");
                        }
                        keyValueMap.Add(arg.Substring(1), args[i + 1]);
                        i++;
                    }
                    else
                    {
                        keyValueMap.Add(arg.Substring(1), "");
                    }
                }
                else
                {
                    throw new ArgumentException("The argument list must have the form (-key value?)* ");
                }
            }
        }

        private bool IsNextArgAValue(IReadOnlyList<string> args, int i)
        {
            return ((i + 1 < args.Count) && (!IsKey(args[i + 1])));
        }

        private bool IsKey(string s)
        {
            return s.StartsWith("-", StringComparison.Ordinal);
        }
    }
    }

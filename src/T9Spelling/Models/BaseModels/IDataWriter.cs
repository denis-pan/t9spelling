﻿namespace T9Spelling.Models
{
    /// <summary>
    ///     Output data writer.
    /// </summary>
    public interface IDataWriter
    {
        /// <summary>
        ///     Write output data.
        /// </summary>
        /// <param name="data">Output data.</param>
        void WriteData(IData data);
    }
}

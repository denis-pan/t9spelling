﻿using System;
using T9SpellingUtils.Models;

namespace T9Spelling.Models
{
    public interface IWorker
    {
        /// <summary>
        ///     Provider for input data.
        /// </summary>
        IDataReader Reader { get; set; }

        /// <summary>
        ///     Provider for output data.
        /// </summary>
        IDataWriter Writer { get; set; }

        /// <summary>
        ///     Keyboard used by the converter.
        /// </summary>
        Keyboard Keyboard { get; }

        /// <summary>
        ///     Input data to be processed.
        /// </summary>
        InputData InputData { get; }

        /// <summary>
        ///     Output data is result of converting(decoding).
        /// </summary>
        OutputData OutputData { get; }

        /// <summary>
        ///     Enable distinguishing lowercase letters from capital letters.
        /// </summary>
        bool CaseSensitive { get; }

        /// <summary>
        ///     Decode input data to message.
        /// </summary>
        bool Decode { get; }

        /// <summary>
        ///     Primary worker initialization before using.
        /// </summary>
        /// <returns>State is initialization success.</returns>
        bool Init();

        /// <summary>
        ///     Start data processing and save result.
        /// </summary>
        void Run();

    }
}

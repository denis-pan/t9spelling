﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("T9SpellingTests")]

namespace T9Spelling.Models
{
    using System;
    using System.Collections.Generic;
    /// <summary>
    ///     Abstract class for parse arguments from command line.
    /// </summary>
    internal abstract class CommandLineArguments
    {
        /// <summary>
        ///     Dictionary for storing pairs of arguments name and value.
        /// </summary>
        private readonly IDictionary<string, string> _keyValueMap =
            new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

        /// <summary>
        ///     Parse arguments from string array to argument object.
        /// </summary>
        /// <param name="args">Arguments from command line.</param>
        protected CommandLineArguments(params string[] args)
        {
            for (var i = 0; i < args.Length; i++)
            {
                var arg = args[i];
                if (IsKey(arg))
                {
                    if (IsNextArgValue(args, i))
                    {
                        if (_keyValueMap.ContainsKey(arg))
                        {
                            throw new ArgumentException($"The key {arg} is contained twice ");
                        }
                        _keyValueMap.Add(arg.Substring(1), args[i + 1]);
                        i++;
                    }
                    else
                    {
                        _keyValueMap.Add(arg.Substring(1), "");
                    }
                }
                else
                {
                    throw new ArgumentException("The argument list must have the form (-key value?)* ");
                }
            }
        }

        /// <summary>
        ///     Count of parsed arguments
        /// </summary>
        public int Count => _keyValueMap.Count;

        /// <summary>
        ///     Indexer for parsed argument.
        /// </summary>
        /// <param name="key">Argument name.</param>
        /// <returns>Argument value.</returns>
        protected string this[string key] => !_keyValueMap.ContainsKey(key) ? null : _keyValueMap[key];

        /// <summary>
        ///     Check is argument has a value.
        /// </summary>
        /// <param name="args">Arguments from command line.</param>
        /// <param name="i">Argument index</param>
        /// <returns>Checking result.</returns>
        private bool IsNextArgValue(IReadOnlyList<string> args, int i)
        {
            return i + 1 < args.Count && !IsKey(args[i + 1]);
        }

        /// <summary>
        ///     Check string is argument or argument's value.
        /// </summary>
        /// <param name="s">Argument or argument value.</param>
        /// <returns>Checking result.</returns>
        private bool IsKey(string s)
        {
            return s.StartsWith("-", StringComparison.Ordinal);
        }
    }
}
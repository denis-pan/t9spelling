﻿namespace T9Spelling.Models
{
    /// <summary>
    ///     Input data reader.
    /// </summary>
    public interface IDataReader
    {
        /// <summary>
        ///     Get input data from current provider.
        /// </summary>
        /// <returns>Input data.</returns>
        IData GetData();
    }
}

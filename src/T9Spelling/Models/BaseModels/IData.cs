﻿namespace T9Spelling.Models
{
    using System.Collections.Generic;

    /// <summary>
    ///     Base data used by convertor.
    /// </summary>
    public interface IData
    {
        /// <summary>
        ///     Count of strings with data
        /// </summary>
        int N { get; }

        /// <summary>
        ///     List with data string.
        /// </summary>
        List<string> Data { get; }
    }
}
﻿namespace T9Spelling.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Runtime.Serialization;
    using Validation;

    /// <summary>
    ///     Input data model.
    /// </summary>
    [DataContract]
    public sealed class InputData : IData
    {
        /// <summary>
        ///     Create input data model from list of input messages.
        /// </summary>
        /// <param name="n">Count of messages in list.</param>
        /// <param name="inputStrings">List of input messages.</param>
        public InputData(int n, List<string> inputStrings)
        {
            N = n;
            Data = inputStrings;
        }

        /// <inheritdoc />
        [Required(ErrorMessage = "N is require")]
        [Range(1, 100, ErrorMessage = "Out of range from 1 to 100")]
        [DataMember]
        public int N { get; private set; }

        /// <inheritdoc />
        [Required(ErrorMessage = "Data is require")]
        [InputStringsValidation("N", ErrorMessage = "Incorrect input strings counts")]
        [DataMember]
        public List<string> Data { get; private set; }
    }
}
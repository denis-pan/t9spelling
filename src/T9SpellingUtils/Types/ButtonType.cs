﻿namespace T9SpellingUtils.Types
{
    /// <summary>
    /// Type of button.
    /// </summary>
    public enum ButtonType
    {
        /// <summary>
        /// Button with symbols.
        /// </summary>
        Standard,
        /// <summary>
        /// Button change case for next symbol.
        /// </summary>
        UpperCase,
        /// <summary>
        /// Button for witch language.
        /// </summary>
        SwitchLang
    }
}

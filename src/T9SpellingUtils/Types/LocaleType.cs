﻿namespace T9SpellingUtils.Types
{
    /// <summary>
    /// Keyboard language.
    /// </summary>
    public enum LocaleType
    {
        None,
        EN,
        RU
    }
}

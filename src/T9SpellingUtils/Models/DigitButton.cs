﻿namespace T9SpellingUtils.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///     Model for button with symbols.
    /// </summary>
    public sealed class DigitButton : Button
    {
        public DigitButton(char key, List<char> symbols) : base(key)
        {
            Symbols = symbols.ConvertAll(char.ToLower);
        }

        /// <summary>
        ///     List of symbols for button
        /// </summary>
        public List<char> Symbols { get; }

        /// <summary>
        ///     Get code of symbol.
        /// </summary>
        /// <param name="symbol">Symbol from current button</param>
        /// <returns>Code of symbol.</returns>
        public string GetCode(char symbol)
        {
            var index = Symbols.IndexOf(symbol);
            if (index >= 0) return new string(Key, index + 1);

            throw new ArgumentOutOfRangeException($"Button '{Key}' haven't symbol '{symbol}'.");
        }

        /// <summary>
        ///     Get symbol by code.
        /// </summary>
        /// <param name="countTouch">Count of touch.</param>
        /// <returns>Parsed symbol.</returns>
        public char GetSymbol(int countTouch)
        {
            if (countTouch > 0 && countTouch <= Symbols.Count) return Symbols[countTouch - 1];

            throw new ArgumentOutOfRangeException($"Button '{Key}' haven't {countTouch} symbols.");
        }
    }
}
﻿namespace T9SpellingUtils.Models
{
    using Types;

    /// <summary>
    ///     Model for button with special action.
    /// </summary>
    public sealed class SpecialButton : Button
    {
        /// <summary>
        ///     Create special button object.
        /// </summary>
        /// <param name="key">Key of the button.</param>
        /// <param name="type">Type of the button</param>
        public SpecialButton(char key, ButtonType type) : base(key, type)
        {

        }
    }
}
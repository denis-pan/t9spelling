﻿namespace T9SpellingUtils.Models
{
    using System.Collections.Generic;
    using Types;

    /// <summary>
    ///     Model for keyboard.
    /// </summary>
    public sealed class Keyboard
    {
        private Keyboard()
        {
        }

        /// <summary>
        ///     Create keyboard object.
        /// </summary>
        /// <param name="locale">Keyboards language.</param>
        /// <param name="buttons">Keyboards buttons.</param>
        public Keyboard(LocaleType locale, List<Button> buttons)
        {
            Buttons = buttons;
            Locale = locale;
        }

        /// <summary>
        ///     Buttons of keyboard.
        /// </summary>
        public List<Button> Buttons { get; private set; }

        /// <summary>
        ///     Keyboard language.
        /// </summary>
        public LocaleType Locale { get; private set; }

        /// <summary>
        ///     Default keyboard model.
        /// </summary>
        public static Keyboard Default { get; } = new Keyboard
        {
            Locale = LocaleType.EN,
            Buttons = new List<Button>
            {
                new DigitButton('0', new List<char> {' '}),
                new DigitButton('2', new List<char> {'a', 'b', 'c'}),
                new DigitButton('3', new List<char> {'d', 'e', 'f'}),
                new DigitButton('4', new List<char> {'g', 'h', 'i'}),
                new DigitButton('5', new List<char> {'j', 'k', 'l'}),
                new DigitButton('6', new List<char> {'m', 'n', 'o'}),
                new DigitButton('7', new List<char> {'p', 'q', 'r', 's'}),
                new DigitButton('8', new List<char> {'t', 'u', 'v'}),
                new DigitButton('9', new List<char> {'w', 'x', 'y', 'z'}),
                new SpecialButton('*', ButtonType.UpperCase)
            }
        };
    }
}
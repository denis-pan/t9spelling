﻿namespace T9SpellingUtils.Models
{
    using Types;

    /// <summary>
    ///     Abstract button class
    /// </summary>
    public abstract class Button
    {
        /// <summary>
        ///     Create button object.
        /// </summary>
        /// <param name="key">Key of the button.</param>
        /// <param name="type">Type of the button</param>
        protected Button(char key, ButtonType type = ButtonType.Standard)
        {
            Key = key;
            Type = type;
        }

        /// <summary>
        ///     Key of the button.
        /// </summary>
        public char Key { get; }

        /// <summary>
        ///     Type of the button.
        /// </summary>
        public ButtonType Type { get; }
    }
}
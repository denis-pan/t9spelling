﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("T9SpellingTests")]
namespace T9SpellingUtils
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Models;
    using Types;

    /// <summary>
    ///     Converter for user messages.
    /// </summary>
    public sealed class Converter
    {
        //TODO Return result as object { Success: true/false; Result: <Results list>; Errors: <Errors list> }. 
        //TODO Methods rename as TryGetCode/TryGetMessage (return bool + out param) 

        /// <summary>
        ///     Create converter for messages.
        /// </summary>
        /// <param name="keyboard">Keyboard model.</param>
        /// <param name="caseSensitive">Case sensitive flag.</param>
        public Converter(Keyboard keyboard, bool caseSensitive = false)
        {
            CurrentKeyboard = keyboard;
            CaseSensitive = caseSensitive;
        }

        private Keyboard CurrentKeyboard { get; }

        private bool CaseSensitive { get; }

        /// <summary>
        ///     Convert messages to code.
        /// </summary>
        /// <param name="messages">List of messages</param>
        /// <returns>List of codes.</returns>
        public List<string> MessageToCode(List<string> messages)
        {
            return ProcessListAsync(MessageToCode, messages);
        }

        /// <summary>
        ///     Convert list of codes to strings.
        /// </summary>
        /// <param name="codes">List of codes.</param>
        /// <returns>Return list of decoded strings.</returns>
        public List<string> CodeToMessage(List<string> codes)
        {
            return ProcessListAsync(CodeToMessage, codes);
        }

        private List<T> ProcessListAsync<T>(Func<T, T> implementer, List<T> list)
        {
            List<Task<T>> taskStorage = new List<Task<T>>();
            foreach (var el in list)
            {
                taskStorage.Add(Task.Run(() => implementer(el)));
            }
            Task.WaitAll(taskStorage.ToArray());

            return taskStorage.Select(t => t.Result).ToList<T>();
        }

        internal string CodeToMessage(string code)
        {
            string result = string.Empty;
            int count = 1;
            bool isUpcase = false;
            for (var i = 0; i < code.Length; i++)
            {
                var symbol = code[i];
                if ((i == code.Length - 1) || (symbol != code[i + 1]))
                {
                    Button btn = CurrentKeyboard.Buttons.FirstOrDefault(b => b.Key == symbol);
                    if (btn != null)
                    {
                        switch (btn.Type)
                        {
                            case ButtonType.Standard:
                                var letter = ((DigitButton) btn).GetSymbol(count);
                                result += isUpcase && CaseSensitive ? char.ToUpper(letter) : letter;
                                isUpcase = false;
                                break;

                            case ButtonType.UpperCase:
                                isUpcase = true;
                                break;
                        }
                    }
                    else
                    {
                        // TODO if symbol != ' ' then  WarningException: "Unexpected symbol"
                    }
                    count = 1;
                }
                else
                {
                    count++;
                }
            }

            return result;
        }

        internal string MessageToCode(string line)
        {
            string result = string.Empty;
            char lastDigit = default(char);
            foreach (char chr in line)
            {
                char symbol = char.ToLower(chr);
                DigitButton btn = (DigitButton)CurrentKeyboard.Buttons.FirstOrDefault(b =>
                        b.Type == ButtonType.Standard &&
                        ((DigitButton)b).Symbols.IndexOf(symbol) >= 0
                    );
                if (btn != null)
                {
                    char digit = btn.Key;
                    if (digit == lastDigit)
                    {
                        result += " ";
                    }
                    if (CaseSensitive && char.IsUpper(chr))
                    {
                        var upcaseBtn = CurrentKeyboard.Buttons.FirstOrDefault(b => b.Type == ButtonType.UpperCase);
                        result += upcaseBtn != null ? upcaseBtn.Key : default(char);
                    }

                    result += btn.GetCode(symbol);

                    lastDigit = digit;
                }
                else
                {
                    //TODO WarningException: "Unexpected symbol"
                }
            }
            return result;
        }
    }
}
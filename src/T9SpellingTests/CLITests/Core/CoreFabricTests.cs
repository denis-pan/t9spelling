﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using T9Spelling.Core;

namespace T9SpellingTests.CLITests.Core
{
    [TestClass]
    public class CoreFabricTests
    {
        [TestMethod]
        public void FabricForWorkerCli()
        {
            //arrange
            var worker = Worker.CreateWorker(new string[] {"-i", "input.json"});

            //act

            //assert
            Assert.IsInstanceOfType(worker, typeof(WorkerCLI));
        }

        [TestMethod]
        public void FabricForWorkerUi()
        {
            //arrange
            var worker = Worker.CreateWorker(new string[] {});

            //act

            //assert
            Assert.IsInstanceOfType(worker, typeof(WorkerUI));
        }
    }
}

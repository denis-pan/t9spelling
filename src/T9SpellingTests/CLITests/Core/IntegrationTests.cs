﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using T9Spelling.Core;
using T9Spelling.Models;
using T9SpellingUtils;
using T9SpellingUtils.Models;

namespace T9SpellingTests.CLITests.Core
{
    [TestClass]
    public class IntegrationTests
    {
        private class WorkerTest : Worker
        {
            public WorkerTest(bool decode = false, bool caseSensitive = false)
            {
                Keyboard = Keyboard.Default;
                Decode = decode;
                CaseSensitive = caseSensitive;

                Converter = new Converter(Keyboard, CaseSensitive);

                var writer = new Mock<IDataWriter>() { CallBase = true };
                writer.Setup(x => x.WriteData(It.IsAny<IData>()));

                Writer = writer.Object;

            }

            public override bool Init()
            {
                return true;
            }

            public override void Run()
            {
                Process();
            }
        }

        [TestMethod]
        public void MessageConvertTest()
        {
            //arrange
            var reader = new Mock<IDataReader>() { CallBase = true };
            reader.Setup(x => x.GetData()).Returns(new InputData(2, new List<string>(){"Abc", "foo  bar" }));

            var worker = new WorkerTest(false, true)
            {
                Reader = reader.Object
            };

            //act
            worker.Run();

            //assert
            CollectionAssert.AreEqual(worker.OutputData.Data, new List<string>() {"*2 22 222", "333666 6660 022 2777"});
        }

        [TestMethod]
        public void CodeConvertTest()
        {
            //arrange
            var reader = new Mock<IDataReader>() { CallBase = true };
            reader.Setup(x => x.GetData()).Returns(new InputData(2, new List<string>() { "*2 22 222", "*4433555 555666096667775553" }));

            var worker = new WorkerTest(true, true)
            {
                Reader = reader.Object
            };

            //act
            worker.Run();

            //assert
            CollectionAssert.AreEqual(worker.OutputData.Data, new List<string>() { "Abc", "Hello world" });
        }

        [TestMethod]
        public void InvalidInputTest()
        {
            //arrange
            var reader = new Mock<IDataReader>() { CallBase = true };
            reader.Setup(x => x.GetData()).Returns(new InputData(1, new List<string>() { "*2 22 222", "*4433555 555666096667775553" }));

            var worker = new WorkerTest(true, true)
            {
                Reader = reader.Object
            };

            //act
            worker.Run();

            //assert
            Assert.AreEqual(worker.OutputData, null);
        }
    }
}

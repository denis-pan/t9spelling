﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using T9Spelling.Models;
using T9Spelling.Validation;
using T9SpellingUtils.Types;

namespace T9SpellingTests.CLITests.ValidationAttributeTests
{
    [TestClass]
    public class KeypadValidationAttributeTests
    {
        private readonly SerializableButton CorrectKey1 = new SerializableButton { Key = '1', Symbols = new List<char>() { 'a', 'b', 'c' } };
        private readonly SerializableButton CorrectKey2 = new SerializableButton { Key = '2', Symbols = new List<char>() { 'd', 'e', 'f' } };
        private readonly SerializableButton DuplicateSymbol = new SerializableButton { Key = '3', Symbols = new List<char>() { 'a', 'e', 'f' } };
        private readonly SerializableButton DuplicateKey = new SerializableButton { Key = '1', Symbols = new List<char>() { 'x', 'y', 'z' } };
        private readonly SerializableButton InvalidKey = new SerializableButton { Key = 'z', Symbols = new List<char>() { 'a', 'b', 'c' } };
        private readonly SerializableButton UpcaseKey = new SerializableButton { Key = '*', Type = ButtonType.UpperCase };

        [TestMethod]
        public void CorrectKeypadTest(){
            //arrange
            var testValidKeypad = new List<SerializableButton>() { CorrectKey1, CorrectKey2, UpcaseKey };
            var keypadValidator = new KeypadValidationAttribute();

            //act
            var result = keypadValidator.IsValid(testValidKeypad);

            //assert
            Assert.AreEqual(result, true);
        }

        [TestMethod]
        public void DuplicateSymbolTest()
        {
            //arrange
            var testInvalidKeypad = new List<SerializableButton>() { CorrectKey1, DuplicateSymbol, UpcaseKey };
            var keypadValidator = new KeypadValidationAttribute();

            //act
            var result = keypadValidator.IsValid(testInvalidKeypad);

            //assert
            Assert.AreEqual(result, false);
        }

        [TestMethod]
        public void DuplicateKeyTest()
        {
            //arrange
            var testInvalidKeypad = new List<SerializableButton>() { CorrectKey1, DuplicateKey, UpcaseKey };
            var keypadValidator = new KeypadValidationAttribute();

            //act
            var result = keypadValidator.IsValid(testInvalidKeypad);

            //assert
            Assert.AreEqual(result, false);
        }

        [TestMethod]
        public void InvalidKeyTest()
        {
            //arrange
            var testInvalidKeypad = new List<SerializableButton>() { CorrectKey1, InvalidKey, UpcaseKey };
            var keypadValidator = new KeypadValidationAttribute();

            //act
            var result = keypadValidator.IsValid(testInvalidKeypad);

            //assert
            Assert.AreEqual(result, false);
        }

    }
}

﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using T9Spelling.Validation;

namespace T9SpellingTests.CLITests.ValidationAttributeTests
{
    [TestClass]
    public class FileAndDirValidationAttributeTests : IDisposable
    {
        private string testDirectoryName = "TestFolder";
        private string testFileName = "TestFile.json";

        public FileAndDirValidationAttributeTests(){
            DirectoryInfo testDir = new DirectoryInfo(testDirectoryName);
            testDir.Create();

            FileStream fs;
            FileInfo testFileWithPath = new FileInfo(testDir.FullName + @"\" + testFileName);
            fs = testFileWithPath.Create();
            fs.Close();
            FileInfo testFileRoot = new FileInfo(testFileName);
            fs = testFileRoot.Create();
            fs.Close();
        }

        [TestMethod]
        public void DirectoryExistsTestForCorrectPath(){
            //arrange
            var dirExistNullable = new DirectoryExistsValidationAttribute(true);
            var dirExist = new DirectoryExistsValidationAttribute();

            //act
            var result = dirExist.IsValid(testDirectoryName + @"\" + testFileName);
            var resultNullable = dirExistNullable.IsValid(testDirectoryName + @"\" + testFileName);
            var resultFileNotExist = dirExist.IsValid(testDirectoryName + @"\FileNotExist.json");

            //assert
            Assert.AreEqual(result, true);
            Assert.AreEqual(resultNullable, true);
            Assert.AreEqual(resultFileNotExist, true);
        }

        [TestMethod]
        public void DirectoryExistsTestForWrongPath()
        {
            //arrange
            var dirExistNullable = new DirectoryExistsValidationAttribute(true);
            var dirExist = new DirectoryExistsValidationAttribute();

            //act
            var result = dirExist.IsValid("DirectoryNotExist" + @"\" + testFileName);
            var resultNullable = dirExistNullable.IsValid("DirectoryNotExist" + @"\" + testFileName);
            var resultDirNotExist = dirExist.IsValid(@"DirectoryNotExist\FileNotExist.json");

            //assert
            Assert.AreEqual(result, false);
            Assert.AreEqual(resultNullable, false);
            Assert.AreEqual(resultDirNotExist, false);
        }

        [TestMethod]
        public void DirectoryExistsTestForNull()
        {
            //arrange
            var dirExistNullable = new DirectoryExistsValidationAttribute(true);
            var dirExist = new DirectoryExistsValidationAttribute();

            //act
            var result = dirExist.IsValid(null);
            var resultNullable = dirExistNullable.IsValid(null);

            //assert
            Assert.AreEqual(result, false);
            Assert.AreEqual(resultNullable, true);
        }

        [TestMethod]
        public void DirectoryExistsTestForRootFile()
        {
            //arrange
            var dirExistNullable = new DirectoryExistsValidationAttribute(true);
            var dirExist = new DirectoryExistsValidationAttribute();

            //act
            var result = dirExist.IsValid(testFileName);
            var resultNullable = dirExistNullable.IsValid(testFileName);

            //assert
            Assert.AreEqual(result, true);
            Assert.AreEqual(resultNullable, true);
        }

        [TestMethod]
        public void FileExistsTestForCorrectPath()
        {
            //arrange
            var fileExistNullable = new FileExistsValidationAttribute(true);
            var fileExist = new FileExistsValidationAttribute();

            //act
            var result = fileExist.IsValid(testDirectoryName + @"\" + testFileName);
            var resultNullable = fileExistNullable.IsValid(testDirectoryName + @"\" + testFileName);

            //assert
            Assert.AreEqual(result, true);
            Assert.AreEqual(resultNullable, true);
        }

        [TestMethod]
        public void FileExistsTestForWrongPath()
        {
            //arrange
            var fileExistNullable = new FileExistsValidationAttribute(true);
            var fileExist = new FileExistsValidationAttribute();

            //act
            var result = fileExist.IsValid(@"..\FileNotExist.json");
            var resultNullable = fileExistNullable.IsValid(@"..\FileNotExist.json");

            //assert
            Assert.AreEqual(result, false);
            Assert.AreEqual(resultNullable, false);
        }

        [TestMethod]
        public void FileExistsTestForNull()
        {
            //arrange
            var fileExistNullable = new FileExistsValidationAttribute(true);
            var fileExist = new FileExistsValidationAttribute();

            //act
            var result = fileExist.IsValid(null);
            var resultNullable = fileExistNullable.IsValid(null);

            //assert
            Assert.AreEqual(result, false);
            Assert.AreEqual(resultNullable, true);
        }

        [TestMethod]
        public void FileExistsTestForRootFile()
        {
            //arrange
            var fileExistNullable = new FileExistsValidationAttribute(true);
            var fileExist = new FileExistsValidationAttribute();

            //act
            var result = fileExist.IsValid(testFileName);
            var resultNullable = fileExistNullable.IsValid(testFileName);

            //assert
            Assert.AreEqual(result, true);
            Assert.AreEqual(resultNullable, true);
        }


        public void Dispose()
        {
            DirectoryInfo testDir = new DirectoryInfo(testDirectoryName);
            testDir.Delete(true);

            FileInfo testFile = new FileInfo(testFileName);
            testFile.Delete();
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using T9Spelling.Models;

namespace T9SpellingTests.CLITests.ModelsTests
{
    [TestClass]
    public class InputArgumentsTests
    {
        //args = new string[] { "-i", "decode.json", "-cs", "-o", "out2.json", "-k", "keyboard.json" };

        [TestMethod]
        public void GetHelpTests()
        {
            //arrange

            //act
            var result = InputArguments.GetHelpMessage();

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(true, result.Length > 0);
        }

        [TestMethod]
        public void KeyDuplicateTest()
        {
            //arrange
            var args = new string[] { "-i", "input1.json", "-i", "input2.json" };

            //act
            Action result = () => new InputArguments(args);

            //assert
            Assert.ThrowsException<ArgumentException>(result);
        }

        [TestMethod]
        public void InvalidKeyTest()
        {
            //arrange
            var args = new string[] { "keyinvalid" };

            //act
            Action result = () => new InputArguments(args);

            //assert
            Assert.ThrowsException<ArgumentException>(result);
        }

        [TestMethod]
        public void InputArgTest()
        {
            //arrange
            var args1 = new InputArguments(new string[] { "-i", "input1.json"});
            var args2 = new InputArguments(new string[] { "-input", "input2.json" });

            //act
            var result1 = args1.InputFile;
            var result2 = args2.InputFile;

            //assert
            Assert.AreEqual("input1.json", result1);
            Assert.AreEqual("input2.json", result2);
        }

        [TestMethod]
        public void KeyboardArgTest()
        {
            //arrange
            var args1 = new InputArguments(new string[] { "-k", "keyboard1.json" });
            var args2 = new InputArguments(new string[] { "-keyboard", "keyboard2.json" });

            //act
            var result1 = args1.Keyboard;
            var result2 = args2.Keyboard;

            //assert
            Assert.AreEqual("keyboard1.json", result1);
            Assert.AreEqual("keyboard2.json", result2);
        }

        [TestMethod]
        public void OutputArgTest()
        {
            //arrange
            var args1 = new InputArguments(new string[] { "-o", "output1.json" });
            var args2 = new InputArguments(new string[] { "-output", "output2.json" });

            //act
            var result1 = args1.OutputFile;
            var result2 = args2.OutputFile;

            //assert
            Assert.AreEqual("output1.json", result1);
            Assert.AreEqual("output2.json", result2);
        }

        [TestMethod]
        public void HelpArgTest()
        {
            //arrange
            var args1 = new InputArguments(new string[] { "-h" });
            var args2 = new InputArguments(new string[] { });

            //act
            var result1 = args1.Help;
            var result2 = args2.Help;

            //assert
            Assert.AreEqual(true, result1);
            Assert.AreEqual(false, result2);
        }

        [TestMethod]
        public void DecodeArgTest()
        {
            //arrange
            var args1 = new InputArguments(new string[] { "-d" });
            var args2 = new InputArguments(new string[] { });

            //act
            var result1 = args1.Decode;
            var result2 = args2.Decode;

            //assert
            Assert.AreEqual(true, result1);
            Assert.AreEqual(false, result2);
        }


        [TestMethod]
        public void CaseSensitiveArgTest()
        {
            //arrange
            var args1 = new InputArguments(new string[] { "-cs" });
            var args2 = new InputArguments(new string[] { });

            //act
            var result1 = args1.CaseSensitive;
            var result2 = args2.CaseSensitive;

            //assert
            Assert.AreEqual(true, result1);
            Assert.AreEqual(false, result2);
        }
    }
}

﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using T9SpellingUtils;
using T9SpellingUtils.Models;
using T9SpellingUtils.Types;

namespace T9SpellingTests.UtilsTests.ConverterTests
{
    [TestClass]
    public class MessageConverterTests
    {
        private readonly Keyboard TestKeyboard = new Keyboard(LocaleType.None, new List<Button>(){
            new DigitButton('1', new List<char>() {'a', 'b', 'c', 'd', 'e', '1' }),
            new DigitButton('2', new List<char>() {'F', '2' }),
            new SpecialButton('3', ButtonType.UpperCase)
        });

        [TestMethod]
        public void OneSymbol()
        {
            //arrange
            var defaultConverter = new Converter(Keyboard.Default, true);
            var customConverter = new Converter(TestKeyboard, true);

            //act
            var resultDefault = defaultConverter.MessageToCode("a");
            var resultCustom = customConverter.MessageToCode("b");

            //assert
            Assert.AreEqual("2", resultDefault, "Failed default keyboard test.");
            Assert.AreEqual("11", resultCustom, "Failed custom keyboard test.");
        }

        [TestMethod]
        public void OneSymbolUpcase()
        {
            //arrange
            var defaultConverter = new Converter(Keyboard.Default, true);
            var customConverter = new Converter(TestKeyboard, true);

            //act
            var resultDefault = defaultConverter.MessageToCode("A");
            var resultCustom = customConverter.MessageToCode("F");

            //assert
            Assert.AreEqual("*2", resultDefault, "Failed default keyboard test.");
            Assert.AreEqual("32", resultCustom, "Failed custom keyboard test.");
        }

        [TestMethod]
        public void UnexpectedSymbol()
        {
            //arrange
            var defaultConverter = new Converter(Keyboard.Default, true);
            var customConverter = new Converter(TestKeyboard, true);

            //act
            var resultDefault = defaultConverter.MessageToCode("~");
            var resultCustom = customConverter.MessageToCode("x");

            //assert
            Assert.AreEqual("", resultDefault, "Failed default keyboard test.");
            Assert.AreEqual("", resultCustom, "Failed custom keyboard test.");
        }

        [TestMethod]
        public void CaseSensitiveFlag(){
            //arrange
            var caseSensitiveEnable = new Converter(Keyboard.Default, true);
            var caseSensitiveDisable = new Converter(Keyboard.Default, false);

            //act
            var resultSensitiveEnable = caseSensitiveEnable.MessageToCode("A");
            var resultSensitiveDisable = caseSensitiveDisable.MessageToCode("A");

            //assert
            Assert.AreEqual("*2", resultSensitiveEnable);
            Assert.AreEqual("2", resultSensitiveDisable);
        }

        [TestMethod]
        public void TestListInputData()
        {
            //arrange
            var testList = new List<string>() 
                { 
                    "hi",
                    "yes", 
                    "foo  bar", 
                    "hello world" 
                };
            var converter = new Converter(Keyboard.Default, false);

            //act
            var actual = converter.MessageToCode(testList);

            //assert
            var expected = new List<string>() 
                { 
                    "44 444", 
                    "999337777",
                    "333666 6660 022 2777", 
                    "4433555 555666096667775553" 
                };
            CollectionAssert.AreEqual(expected, actual);
        }
    }
}

﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using T9SpellingUtils;
using T9SpellingUtils.Models;
using T9SpellingUtils.Types;

namespace T9SpellingTests.UtilsTests.ConverterTests
{
    [TestClass]
    public class CodeConverterTests
    {
        private Keyboard TestKeyboard = new Keyboard(LocaleType.None, new List<Button>(){
            new DigitButton('1', new List<char>() {'a', 'b', 'c', '1' }),
            new DigitButton('2', new List<char>() {'d', 'e', 'f', '2' }),
            new DigitButton('3', new List<char>() {'g', 'h', 'i', '3' }),
            new SpecialButton('4', ButtonType.UpperCase)
        });

        [TestMethod]
        public void OneSymbol()
        {
            //arrange
            var defaultConverter = new Converter(Keyboard.Default, true);
            var customConverter = new Converter(TestKeyboard, true);

            //act
            var resultDefault = defaultConverter.CodeToMessage("22");
            var resultCustom = customConverter.CodeToMessage("3333");

            //assert
            Assert.AreEqual("b", resultDefault, "Failed default keyboard test.");
            Assert.AreEqual("3", resultCustom, "Failed custom keyboard test.");
        }

        [TestMethod]
        public void OneSymbolUpcase()
        {
            //arrange
            var defaultConverter = new Converter(Keyboard.Default, true);
            var customConverter = new Converter(TestKeyboard, true);

            //act
            var resultDefault = defaultConverter.CodeToMessage("*3");
            var resultCustom = customConverter.CodeToMessage("422");

            //assert
            Assert.AreEqual("D", resultDefault, "Failed default keyboard test.");
            Assert.AreEqual("E", resultCustom, "Failed custom keyboard test.");
        }

        [TestMethod]
        public void UnexpectedSymbol()
        {
            //arrange
            var defaultConverter = new Converter(Keyboard.Default, true);
            var customConverter = new Converter(TestKeyboard, true);

            //act
            var resultDefault = defaultConverter.CodeToMessage("#");
            var resultCustom = customConverter.CodeToMessage("55");

            //assert
            Assert.AreEqual("", resultDefault, "Failed default keyboard test.");
            Assert.AreEqual("", resultCustom, "Failed custom keyboard test.");
        }

        [TestMethod]
        public void CaseSensitiveFlag()
        {
            //arrange
            var caseSensitiveEnable = new Converter(Keyboard.Default, true);
            var caseSensitiveDisable = new Converter(Keyboard.Default, false);

            //act
            var resultSensitiveEnable = caseSensitiveEnable.CodeToMessage("*2");
            var resultSensitiveDisable = caseSensitiveDisable.CodeToMessage("*2");

            //assert
            Assert.AreEqual("A", resultSensitiveEnable);
            Assert.AreEqual("a", resultSensitiveDisable);
        }

        [TestMethod]
        public void TestListInputData()
        {
            //arrange
            var testList = new List<string>()
                {
                    "44 444",
                    "999337777",
                    "333666 6660 022 2777",
                    "4433555 555666096667775553"
                };
            var converter = new Converter(Keyboard.Default, false);

            //act
            var actual = converter.CodeToMessage(testList);

            //assert
            var expected = new List<string>()
                {
                    "hi",
                    "yes",
                    "foo  bar",
                    "hello world"
                };

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using T9SpellingUtils.Models;

namespace T9SpellingTests.UtilsTests.ModelsTests
{
    [TestClass]
    public class ButtonTests
    {
        private readonly DigitButton TestButton = new DigitButton('1', new List<char>() { 'a', 'b', 'c' });

        [TestMethod]
        public void GetCode()
        {
            //arrange
            DigitButton digitButton = TestButton;

            //act
            var resultA = digitButton.GetCode('a');
            var resultB = digitButton.GetCode('b');
            var resultC = digitButton.GetCode('c');
            Func<object> resultException = () => digitButton.GetCode('q');


            //assert
            Assert.AreEqual("1", resultA);
            Assert.AreEqual("11", resultB);
            Assert.AreEqual("111", resultC);
            Assert.ThrowsException<ArgumentOutOfRangeException>(resultException);
        }

        [TestMethod]
        public void GetSymbol()
        {
            //arrange
            DigitButton digitButton = TestButton;

            //act
            var resultA = digitButton.GetSymbol(1);
            var resultB = digitButton.GetSymbol(2);
            var resultC = digitButton.GetSymbol(3);
            Func<object> resultException = () => digitButton.GetSymbol(-1);

            //assert
            Assert.AreEqual('a', resultA);
            Assert.AreEqual('b', resultB);
            Assert.AreEqual('c', resultC);
            Assert.ThrowsException<ArgumentOutOfRangeException>(resultException);
        }

    }
}

﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using T9SpellingUtils.Models;
using T9SpellingUtils.Types;

namespace T9SpellingTests.UtilsTests.ModelsTests
{
    [TestClass]
    public class KeyboardTests
    {
        [TestMethod]
        public void DefaultKeyboard()
        {
            //arrange
            var buttons = Keyboard.Default.Buttons.Select(x => x.Key).ToList();
            var symbols = Keyboard.Default.Buttons
                            .Where(x => x.Type == ButtonType.Standard)
                            .SelectMany(x => ((DigitButton)x).Symbols)
                            .ToList();
            var upperCaseButtonCount = Keyboard.Default.Buttons.Count(x => x.Type == ButtonType.UpperCase);

            //act

            //assert
            Assert.IsFalse(HasDuplicate(buttons), "List of buttons has duplicated keys for default keyboard.");
            Assert.IsFalse(HasDuplicate(symbols), "List of buttons has duplicated symbols for default keyboard.");
            Assert.IsTrue(upperCaseButtonCount == 1, "Default keyboard have not upper case button");
        }

        private bool HasDuplicate<T>(List<T> list)
        {
            return list.GroupBy(n => n).Any(c => c.Count() > 1);
        }
    }
}
